//			Nataniel Gaasoy, 131390
//			16HBPROGA
//			REA1121 Matte for Programmering

//	Problem 1.
//	Implement the Midpoint displacement algorithm and draw a horizon
//	on the screen.


#include <SFML/Graphics.hpp>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <random>

std::random_device rd;
std::mt19937 gen(rd());
std::uniform_real_distribution<> dist(-20, 20);

const int POWER = 128;				//	value must be power of 2
//	if power is later input, can use: 
//	power = pow(2, ceil(log(width) / (log(2))));
//	to make sure that the input is closest to the power of 2
const int WINDOWWIDTH = 512 * 2;
const int WINDOWHEIGHT = 512;

int data[POWER];	//	the array of values, y value for every x value

sf::Vertex* horizon(float roughness, float displace, sf::RenderWindow& w)				
{

    data[0] = WINDOWHEIGHT / 2;		//	start on the middle of the window
    data[POWER] = WINDOWHEIGHT / 2;	//	end on the middle of the window

    for (int i = 1; i <= POWER; i *= 2)
    {
        for (int j = (POWER / i) / 2; j < POWER; j += POWER / i)
        {
            data[j] = ((data[j - (POWER / i) / 2] + data[j +
                (POWER / i) / 2]) / 2);
            data[j] += (dist(rd)*displace * 2) - displace;
        }
        displace *= roughness;
    }

    sf::Vertex* line = new sf::Vertex[POWER + 1];
    for (int i = 0; i < POWER; i++)
    {
        line[i].position = sf::Vector2f(i * 8.5F, data[i]),
            line[i].color = sf::Color::Yellow;
    }
    w.draw(line, POWER, sf::LineStrip);
    return line;
}

int main()
{
    sf::RenderWindow window(sf::VideoMode(WINDOWWIDTH, WINDOWHEIGHT), "Example 1");

    float roughness = 0.6f;
    float displacement = 7.0f;

    sf::Vertex* line = horizon(roughness, displacement, window);

    sf::Clock clock;

    // reset timer to 0
    clock.restart();
    float t = 0;

    // Game loop - runs until you close the window
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            // Note: We will be drawing once to a closed window - but nothing bad seems to happen.
            if (event.type == sf::Event::Closed)
                window.close();
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))				// ESC to quit.
            {
                window.close();
            }
        }
        float dt = clock.restart().asSeconds();

        window.clear(sf::Color::Black);
        window.draw(line, POWER, sf::LineStrip);
        window.display();
    }

    return 0;
}
