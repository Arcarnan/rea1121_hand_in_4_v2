//			Nataniel Gaasoy, 131390
//			16HBPROGA
//			REA1121 Matte for Programmering


//	Problem 2.
//	Implement the diamond square algorithm to create a heightmap.
//	Then convert your height values to colour values, put the 
//	colour values into a texture and draw the texture on screen


#include <SFML/Graphics.hpp>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <time.h>
#include <random>

std::random_device rd;
std::mt19937 gen(rd());
std::uniform_real_distribution<> dist(-500, 500);

const int WINDOWWIDTH = 512;
const int WINDOWLENGTH = 512;
const int size = 512 + 1;		//	2^n+1

float grid[size][size];	//	program could not handle the size in a function
						//	so we are getting some really messy code here

void diamondSquares(int offset, sf::RenderWindow& window)
{

	grid[0][0] = offset * 2;				//	set initial value of top left corner
	grid[size - 1][0] = offset * 2;			//	set initial value of top right corner
	grid[0][size - 1] = offset * 2;			//	set initial value of lower leftcorner
	grid[size - 1][size - 1] = offset * 2;	//	set initial value of lower right corner

					//	goes through the whole grid, untill size is too small
	int halfSize;

	for (int sideLength = size - 1; sideLength >= 2; sideLength *= 0.5f, offset *= 0.5f)
	{
		halfSize = sideLength*0.5f;	//	half of the size, to find the middle

					//	generate the square values

		for (int x = 0; x < size - 1; x += sideLength)
		{
			for (int y = 0; y < size - 1; y += sideLength)
			{
				float avg = grid[x][y] +					//	top left
					grid[x + sideLength][y] +				//	top right
					grid[x][y + sideLength] +				//	lower left
					grid[x + sideLength][y + sideLength];	//	lower right
				avg /= 4;

				avg = avg + ((rand() % offset) * 2) - offset;
				grid[x + halfSize][y + halfSize] = avg;
			}		//	center is average of the four sides, pluss a random value
		}

					//	generate the diamond values

		for (int x = 0; x < size; x += halfSize)
		{								//	remove -1 if not wrapping
			for (int y = (x + halfSize) % sideLength; y < size; y += sideLength)
			{							//	remove -1 if not wrapping, need mod for wrapping
				float avg = grid[(x - halfSize + size) % size][y] +	//	left of center
					grid[(x + halfSize) % size][y] +				//	right of center
					grid[x][(y + halfSize) % size] +				//	below center
					grid[x][(y - halfSize + size) % size];			//	above center
				avg /= 4;

				avg = avg + ((rand() % offset) * 2) - offset;	//	avg = avg + (dist(rd)*2 * offset) - offset;
				grid[x][y] = avg;

				//	need these for wrapping, 
				//	remove and adjust loops for non-wrapping
				if (x == 0)
				{
					grid[size - 1][y] = avg;
				}
				if (y == 0)
				{
					grid[x][size - 1] = avg;
				}
			}
		}
	}
}

int main()
{
	srand(time(nullptr));		//	to seed the random function
	int offset = 500;

	sf::RenderWindow window(sf::VideoMode(WINDOWWIDTH, WINDOWLENGTH), "Diamond Square");

	sf::Clock clock;
	clock.restart();		//restart clock when program starts
	float t = 0;			//	starts time at 0

	diamondSquares(offset, window);

	std::cout << "\ngrid[0][0]: " << grid[0][0];			//	cout for testing purposes
	std::cout << "\ngrid[0][256]: " << grid[0][256];		//	cout for testing purposes
	std::cout << "\ngrid[256][0]: " << grid[256][0];		//	cout for testing purposes
	std::cout << "\ngrid[512][256]: " << grid[512][256];	//	cout for testing purposes
	std::cout << "\ngrid[256][512]: " << grid[256][512];	//	cout for testing purposes
	std::cout << "\ngrid[256][256]: " << grid[256][256];	//	cout for testing purposes


								//	Texture code mostly provided by Bernt from fronter
								//	Example 2: A texture as an array of RGBA
	sf::Texture texture;
	texture.create(WINDOWWIDTH, WINDOWLENGTH);
	sf::Uint8* pixels = new sf::Uint8[WINDOWWIDTH * WINDOWLENGTH * 4]; // * 4 because pixels have 4 components (RGBA)
	for (int x = 0; x < WINDOWWIDTH + 1; x++)
	{
		for (int y = 0; y < WINDOWLENGTH - 1; y++)
		{
			if (grid[x][y] > 1100)
			{
				pixels[(x + y * 512) * 4] = 255;		//	red value
				pixels[(x + y * 512) * 4 + 1] = 255;	//	green value
				pixels[(x + y * 512) * 4 + 2] = 255;	//	blue value
				pixels[(x + y * 512) * 4 + 3] = 255;	//	alpha value/ transparancy
			}
			else if (grid[x][y] > 900 && grid[x][y] < 1100)
			{
				pixels[(x + y * 512) * 4] = 160;		//	red value
				pixels[(x + y * 512) * 4 + 1] = 160;	//	green value
				pixels[(x + y * 512) * 4 + 2] = 160;	//	blue value
				pixels[(x + y * 512) * 4 + 3] = 255;	//	alpha value/ transparancy
			}
			else if (grid[x][y] > 500 && grid[x][y] < 900)
			{
				pixels[(x + y * 512) * 4] = 0;			//	red value
				pixels[(x + y * 512) * 4 + 1] = 255;	//	green value
				pixels[(x + y * 512) * 4 + 2] = 0;		//	blue value
				pixels[(x + y * 512) * 4 + 3] = 255;	//	alpha value/ transparancy
			}
			else
			{
				pixels[(x + y * 512) * 4] = 0;			//	red value
				pixels[(x + y * 512) * 4 + 1] = 0;		//	green value
				pixels[(x + y * 512) * 4 + 2] = 255;	//	blue value
				pixels[(x + y * 512) * 4 + 3] = 255;	//	alpha value/ transparanc
			}
		}
	}
	texture.update(pixels);

	sf::Sprite sprite;
	sprite.setTexture(texture);
	sprite.setPosition(0, 0);

	diamondSquares(offset, window);
	while (window.isOpen())
	{
		sf::Event event;

		while (window.pollEvent(event))
		{
			// "close requested" event: we close the window
			if (event.type == sf::Event::Closed)
				window.close();
		}

		t += clock.restart().asSeconds();

		window.clear(sf::Color::Black);
		window.draw(sprite);
		window.display();
	}
	return 0;
}

//	The diamond - square algorithm begins with a 2D square array of width and height 2n + 1. The four corner points 
//	of the array must firstly be set to initial values.The diamond and square steps are then performed alternately 
//	until all array values have been set. The diamond step : For each square in the array, set the midpoint of that 
//	square to be the average of the four corner points plus a random value.The square step : For each diamond in the 
//	array, set the midpoint of that diamond to be the average of the four corner points plus a random value. At each 
//	iteration, the magnitude of the random value should be reduced.

//	The diamond step : Taking a square of four points, generate a random value at the square midpoint, where the two 
//	diagonals meet.The midpoint value is calculated by averaging the four corner values, plus a random amount.This 
//	gives you diamonds when you have multiple squares arranged in a grid.
//	
//	The square step : Taking each diamond of four points, generate a random value at the center of the diamond.
//	Calculate the midpoint value by averaging the corner values, plus a random amount generated in the same range 
//	as used for the diamond step.This gives you squares again.