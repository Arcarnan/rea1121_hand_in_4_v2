//			Nataniel Gaasoy, 131390
//			16HBPROGA
//			REA1121 Matte for Programmering


//	Problem 3. 
//	a) Implement Boids in 2d and animate their movement on the screen.
//	b) How many boids can you put on your screen before your program
//	starts having problems ?
//	c) Do you have any ideas how you could optimise your code ? Include
//	brief explanations of what you think might improve your implementation.
//	No implementations necessary.

#include <SFML/Graphics.hpp>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <ctime>
#include <random>
                                               
const int WINDOWWIDTH = 1024;                  
const int WINDOWHEIGHT = 1024;                 
const float WEIGHT = 10.0;            //  how hard it moves towards center of mass
const float MAXVEL = 120.0f;         //  maximum velocity
const int MAXBOIDS = 200;            //  number of boids
const int AVOIDANCESIZE = 20;        //  distance between boids
const int NEIGHBOURDISTANCE = 70;    // distance to neighbour

std::random_device rd;
std::mt19937 gen(rd());
std::uniform_real_distribution<> dist(0, 0);

void randomPosition(sf::Vector2f & pos)
{
    pos.x = static_cast<float>(rand() % WINDOWWIDTH);
    pos.y = static_cast<float>(rand() % WINDOWHEIGHT);
}

float abs(sf::Vector2f v)
{
    return (sqrt(v.x*v.x + v.y*v.y));
}

class Boid
{
public:
    sf::Vector2f pos;                   //  position
    sf::Vector2f vel;                   //  velocity
    sf::CircleShape shape;              //  shape
    sf::CircleShape rangeMarker;        //  Jone's border marker
    float shapeSize;                    //  size of boid

    Boid();
    void draw(sf::RenderWindow& w);
    //void addBoid();
    //void addBoid (float mouseX, float mouseY);

};

class Flock
{
public:
    Boid boids[MAXBOIDS];
    void update(float dt);
    void draw(sf::RenderWindow& w);

    sf::Vector2f centerOfMass(int j)
    {
        int neighbours = 0;
        sf::Vector2f cm (0, 0);                               //  cm = center of mass
        
        for (int i = 0; i < MAXBOIDS; i++)                    //  cm = (b1.position + b2.position + ... 
        {                                                     //       + bN.position) / N
            if (i != j && (abs(boids[i].pos - boids[j].pos)) < NEIGHBOURDISTANCE)
            {                                                 
                cm = cm + boids[i].pos;
                neighbours++;
            }
        }                                                     //    N - 1 gives percieved center of mass
        //cm = cm / (MAXBOIDS - 1.0f);                          //    the center for all other than itself
        cm = cm / (neighbours - 1.0f);
        return ((cm - boids[j].pos)*(WEIGHT/ 100));
    }

    sf::Vector2f avoidCollision(int j)
    {
        sf::Vector2f av(0, 0);                    //  avoidance vector
        for (int i = 0; i < MAXBOIDS; i++)        //   if two boids are too close
        {                                         //  make them move away from each other
            if (i != j && (abs(boids[i].pos - boids[j].pos)) < AVOIDANCESIZE)
            {
                av = av - (boids[i].pos - boids[j].pos);
            }
        }
        return (av*WEIGHT);
    }

    sf::Vector2f alignment(int j)
    {
        int neighbours = 0;
        sf::Vector2f pv(0, 0);                      //  percievedVelocity
       
        for (int i = 0; i < MAXBOIDS; i++)          //  averaging the velocities
        {
            if (i != j && (abs(boids[i].pos - boids[j].pos)) < NEIGHBOURDISTANCE)
            {
                pv = pv + boids[i].vel;
                neighbours++;
            }
        }
        pv = pv / (neighbours - 1.0f);
        return ((pv - boids[j].vel) / 8.0f);
    }
};

Boid::Boid()
{
    shapeSize = 5;
    pos = sf::Vector2f(0, 0);
    vel = sf::Vector2f(MAXVEL, MAXVEL);
    randomPosition(pos);
    shape.setFillColor(sf::Color::Yellow);

    std::cout << "\nNew boid with pos: " << pos.x << ", " << pos.y;
}

void Boid::draw(sf::RenderWindow& w)
{
    shape.setPointCount(6);
    shape.setPosition(pos);
    shape.setOrigin(shapeSize, shapeSize);
    shape.setRadius(shapeSize);
    w.draw(shape);

    rangeMarker.setOrigin(sf::Vector2f(NEIGHBOURDISTANCE, NEIGHBOURDISTANCE));  //  Jone's border marker
    rangeMarker.setFillColor(sf::Color(255, 100, 10, 20));                      //  Jone's border marker
    rangeMarker.setOutlineColor(sf::Color(255, 255, 0, 23));                    //  Jone's border marker
    rangeMarker.setRadius(NEIGHBOURDISTANCE);                                   //  Jone's border marker
    rangeMarker.setPosition(pos);                                               //  Jone's border marker
    w.draw(rangeMarker);                                                        //  Jone's border marker
}

// Add a new boid into the System from mouse click
//  void Boid::addBoid()
//  {
//      new Boid(mouseX, mouseY));
//  }

void Flock::update(float dt)
{
    for (int i = 0; i < MAXBOIDS; i++)
    {
        sf::Vector2f rule1(0, 0);
        sf::Vector2f rule2(0, 0);
        sf::Vector2f rule3(0, 0);
        //boids[i].update(dt);

        rule1 = centerOfMass(i);
        rule2 = avoidCollision(i);
        rule3 = alignment(i);
        boids[i].vel = boids[i].vel + rule1 + rule2 + rule3*dt;     //  update velocity according to time, dt
        boids[i].pos = boids[i].pos + boids[i].vel*dt;              //  update position according to time, dt

        if (abs(boids[i].vel) > MAXVEL)     //  if boid moves faster than MAXVEL
        {                                   //  set vel to MAXVEL
            boids[i].vel = (boids[i].vel / abs(boids[i].vel)*MAXVEL);
        }
    }
}

void Flock::draw(sf::RenderWindow& w)
{
    for (int i = 0; i < MAXBOIDS; i++)
    {
        boids[i].draw(w);
    }
}

int main()
{
    srand(time(nullptr));
    sf::RenderWindow window(sf::VideoMode(WINDOWWIDTH, WINDOWHEIGHT), "Boids");
    ////window.setFramerateLimit(50);

    sf::Clock clock;
    clock.restart();		//	restart clock when program starts
    float time = 0;			//	starts time at 0

    Flock boids;

    while (window.isOpen())
    {
        sf::Event event;

        while (window.pollEvent(event))
        {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed)
            {
                window.close();
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))				// ESC to quit.
            {
                window.close();
            }
        }

        float dt = clock.restart().asSeconds();

        time += clock.restart().asSeconds();

        window.clear(sf::Color::Black);		// Clear buffer, black background

        boids.draw(window);
        boids.update(dt);
        window.display();
    }
    return 0;
}
