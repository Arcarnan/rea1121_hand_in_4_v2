#include <SFML/Graphics.hpp>		//for Graphics stuff
#include <iostream>			//Used for printing to screen(debug) and some input.
#include <cstdlib>			//random values.
#include <sstream>




const float windowHeight = 450;
const float windowWidth = 800;
const int randomrange = 20;	//range of pixles added to to displacements.
const float d = 60;			//added to all the displacements.


sf::ContextSettings settings;					//The settings Context, all the config info the window needs to run.
sf::RenderWindow window;						//The window that draws the graphics on the screen.
sf::Font font;									//The font imported from file used to print text on Screen.
sf::Vertex* line;
sf::Text disp;
int points = 3;



void drawScreen()
{

	std::stringstream convert;
	convert << (points/2);			//converting int weight to a string that can be printed.
	disp.setString("Iterations " + convert.str() + "\n\n\n\nArrowkeys to change iterations");
	//////////////		Screen :drawing		///////////////
		window.clear();													//Clears the canvas.
		window.draw(disp);
		window.draw(line, points, sf::LinesStrip);
		window.display();												//Sends the buffer to the display.
		//////////////		Screen drawn		///////////////
}

void createLine()
{
	line = new sf::Vertex[points];

	for(int i = 0; i < points; i++)
	{
		line[i] = sf::Vector2f(((windowWidth/(points-1))*i), windowHeight/2);		//set the positions of all the points.
	}


	line[points/2].position.y += -d + float(randomrange) - float((std::rand()%randomrange*2));

	for(int i = 0; i < points/2; i++)	//runs for halv the amount of points on the line, except the start and end point.
	{
		line[points/2-i].position.y = (line[points/2-i-1].position.y + line[points/2-i+1].position.y) /2;
		line[points/2-i].position.y += -d + float(randomrange - float((std::rand()%randomrange*2)));

		line[points/2+i].position.y = (line[points/2+i-1].position.y + line[points/2+i+1].position.y) /2;
		line[points/2+i].position.y += -d + float(randomrange) - float((std::rand()%randomrange*2));
	}

}


int main ()		  														
{
	std::rand();											//Seed with system time.

	window.create(sf::VideoMode(windowWidth, windowHeight), "TITLE", //opens the window and sets the size.
						 sf::Style::Titlebar | sf::Style::Close);	
	window.setFramerateLimit(50);


	if(!font.loadFromFile("FONT.ttf"))					//Loads font from file. Gives error in console if fails.
	{
		std::cout << "\nUnable to load font  \"FONT.fft\".";
	}


	disp.setCharacterSize(50);
	disp.setColor(sf::Color::Yellow);
	disp.setPosition(sf::Vector2f(windowWidth/20, windowHeight/40));
	disp.setFont(font);	

	createLine();
	drawScreen();



	//update()
	while(window.isOpen())
	{
		sf::Event event;
		while(window.pollEvent(event))									
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))				// ESC to quit.
			{
				window.close();
			}
			if(event.type == sf::Event::Closed)								//If the event happening is closed:
			{															//then close the window as well.
				window.close();
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))				// ESC to quit.
			{
				points += 6;
				createLine();
				drawScreen();
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && points > 3)				// ESC to quit.
			{
				points -= 6;
				createLine();
				drawScreen();
			}
		}
	} 

	std::cout << "\n\n";				
	return 0;
}

