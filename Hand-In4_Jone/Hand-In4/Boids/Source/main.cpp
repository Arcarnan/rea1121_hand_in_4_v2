#include <SFML/Graphics.hpp>		//for Graphics stuff
#include <iostream>			//Used for printing to screen(debug) and some input.
#include <cstdlib>			//rand().
#include <cmath>			//abs()
#include <iomanip>			//setW

const int windowHeight =1000;
const int windowWidth = 1000;
const int buttonTimeout = 5;

sf::ContextSettings settings;					//The settings Context, all the config info the window needs to run.
sf::RenderWindow window;						//The window that draws the graphics on the screen.
sf::Font font;
sf::Clock deltatime;					//Time to count deltatime.
float dt;
int timot = 0;
int numOfBoidsNow = 0;
									//The font imported from file used to print text on Screen.
const float bodyRadius = 12;
const int watchRange = 150;
const float maxVel = 100;
const int numberToBeFlock = 4;
const int birdCount = 80;

const int noiseAmount = 8;

const float alignRange = 150;
//const float maxAlign = 30;
const float alignForce = 3.2f;

const int gravityRange = 150;
//const float maxGravity = 30;
const float gravityForce = 1.6f;

const float repultionRange = 100;
//const float maxRepultion = 80;
const float repultionForce = 20;


const float accelerationMultiplier = 0.4f;		//Most powerfull variable. set to 0 is the same as dissableing all the rules.


bool noise      = false;
bool align		= false;
bool gravity 	= false;
bool repultion 	= false;



void randomPosition(sf::Vector2f & pos)
{
	pos.x = static_cast<float>(rand() % windowWidth);
	pos.y = static_cast<float>(rand() % windowHeight);
}

sf::Vector2f clampVector(sf::Vector2f v, float clamp)
{
	sf::Vector2f n;
	float magnitude = std::sqrt(std::pow(v.x, 2) + std::pow(v.y, 2)) + 0.0000001f;
	n.x = (v.x/magnitude) * clamp;
	n.y = (v.y/magnitude) * clamp;
	return n;
}

float magnitude(sf::Vector2f v1, sf::Vector2f v2)										//Will return the distance between to points(x,y)
{
	return std::sqrt( std::pow(v1.x - v2.x,  2)
			   + std::pow(v1.y - v2.y,  2));	
}


float getAngle(sf::Vector2f v1 , sf::Vector2f v2)										//Retuns the angle between two points.
{
	float angle = 0;

	float deltaX = v1.x - v2.x;
	float deltaY = v2.y - v1.y;

	angle = std::atan2(deltaX, deltaY);
	angle = (angle * 180) / 3.14f;

	return ((angle < 0) ? (360 + angle + 90) : angle + 90);
}


bool isWithinRange(sf::Vector2f v1, sf::Vector2f v2)
{
    bool isInRange = false;
    sf::Vector2f distanceVector(0, 0);

 
    if (std::abs(v1.x - v2.x) < std::abs(v1.x - (v2.x - windowWidth)) && std::abs(v1.x - v2.x) < std::abs((v1.x - windowWidth) - (v2.x))) 
    {
        distanceVector.x = v1.x - v2.x;
    }

    if (std::abs(v1.y - v2.y) < std::abs(v1.y - (v2.y - windowWidth)) && std::abs(v1.y - v2.y) < std::abs((v1.y - windowWidth) - (v2.y)))  
    {
        distanceVector.y = v1.y - v2.y;
    }



    if (magnitude(sf::Vector2f(0, 0), distanceVector) < watchRange)
    {
        isInRange = true;
    }

    return isInRange;
}


sf::Vector2f shortestPath(sf::Vector2f v1, sf::Vector2f v2)
{
    sf::Vector2f distanceVector(0, 0);

    if (std::abs(v1.x - v2.x) < std::abs(v1.x - (v2.x - windowWidth)) && std::abs(v1.x - v2.x) < std::abs((v1.x - windowWidth) - (v2.x)))
    {
        distanceVector.x = v2.x;
    }

    if (std::abs(v1.y - v2.y) < std::abs(v1.y - (v2.y - windowWidth)) && std::abs(v1.y - v2.y) < std::abs((v1.y - windowWidth) - (v2.y)))
    {
        distanceVector.y = v2.y;
    }

    return distanceVector;
}


class Boid
{
	private:
		sf::Vector2f pos;
		sf::Vector2f vel;
		sf::CircleShape body;
		sf::CircleShape rangeMarker;
		int id;


	public:
		Boid()
		{	
			id = numOfBoidsNow;
			numOfBoidsNow++;
			pos = sf::Vector2f(0, 0);
			vel = sf::Vector2f(0, maxVel);
			randomPosition(pos);


			body.setPointCount(3);
			body.setOrigin(sf::Vector2f(bodyRadius, bodyRadius));
			body.setFillColor(sf::Color::Red);
			body.setRadius(bodyRadius);
			body.setScale(sf::Vector2f(1, 1.5f));


            if (id == 0 || id == birdCount - 1)
            {
                body.setOutlineThickness(3);
                if (id == 0)
                    body.setOutlineColor(sf::Color::Magenta);
                else
                    body.setOutlineColor(sf::Color::Cyan);

            }


			rangeMarker.setOrigin(sf::Vector2f(repultionRange, repultionRange));			
			rangeMarker.setFillColor(sf::Color(255, 100, 10, 20));
			rangeMarker.setOutlineColor(sf::Color(255, 255, 0, 23));
			rangeMarker.setRadius(repultionRange);
			rangeMarker.setOutlineThickness(gravityRange - repultionRange);
			rangeMarker.setPointCount(12);

			std::cout << "\nNew boid with pos: " << pos.x << ", " << pos.y;
		}


		void update(int inSight, sf::Vector2f* birdWatch, sf::Vector2f* birdFlight)			//Recieves a pointer to an array with all the boids pos and vel within range.
		{
			//std::cout << "\ninSight: " << inSight;
			sf::Vector2f alignVec(0, 0);		//Movement to match other boid's velcoities.
			sf::Vector2f gravityVec(0, 0);		//Movement to approach other boids.
			sf::Vector2f repultionVec(0, 0);	//Movement to avoid collisjon with other boids.
			sf::Vector2f avoidVec(0, 0);		//Movement to avoid obsticles.

			sf::Color updateColor;


            /*
			if (inSight == 0) updateColor = sf::Color::Red;
			if (inSight == 1) updateColor = sf::Color::Yellow;
			if (inSight == 2) updateColor = sf::Color::Green;
			if (inSight == 3) updateColor = sf::Color::Blue;
			if (inSight > 3) updateColor = sf::Color::White;
            */
            updateColor = sf::Color(255 - 255 * (inSight / float(birdCount)), 255 + 255 * (inSight / float(birdCount)), 0, 255);
			body.setFillColor(updateColor);


			if(pos.x > windowWidth)	pos.x = 0;
			if(pos.y > windowHeight)pos.y = 0;

			if(pos.x < 0)	pos.x = windowWidth;
			if(pos.y < 0)	pos.y = windowHeight;
			

			if(noise) 										//Add a random noise to the boid, so it moves around.
			{	
				int angle = rand()%360;						//Choose random angle to add to vel:
				vel.x += static_cast<float>(::sin(angle)*noiseAmount * accelerationMultiplier);
				vel.y += static_cast<float>(std::cos(angle)*noiseAmount * accelerationMultiplier);
			}



			for (int b = 0; b < inSight; b++)				//Gravitates to other boids.		
			{
				float magn = 0;
				
                magn = magnitude(pos, birdWatch[b]);


				if (magn < repultionRange)
				{
                    if(repultion)
					    repultionVec += clampVector((pos - birdWatch[b]), 1) * repultionForce * (repultionRange - magn*1.2f);
				}
                else
                {
                    
                     if (magn < gravityRange && gravity)
                            gravityVec += clampVector((birdWatch[b] - pos), 1) *gravityForce * (gravityRange + magn);
                    

                     if (magn < alignRange && align)
                        alignVec += (clampVector((birdFlight[b]), 1) *alignForce * (alignRange + magn));
                }
				
			}
			/*std::cout	<< std::setw(6) << "\nID: "<< id
						<< std::setw(14) << "Dir: " << getAngle(sf::Vector2f(0,0), vel) 
						<< std::setw(14) <<"Aln: " << magnitude(sf::Vector2f(0,0), alignVec)
						<< std::setw(14) << "grv: " << magnitude(sf::Vector2f(0, 0), gravityVec)
						<< std::setw(14) << "rpl: " << magnitude(sf::Vector2f(0, 0), repultionVec)
						<< std::setw(18) << "InSight: " << inSight;
						*/
			vel += gravityVec * accelerationMultiplier;
			vel += repultionVec * accelerationMultiplier;
			vel += alignVec * accelerationMultiplier;
			
			body.setRotation(getAngle(sf::Vector2f(0, 0), vel) + 90);
			if (inSight < numberToBeFlock) 
                    vel = clampVector(vel, maxVel + (maxVel/100) * 2);					//Clamp vel to be max vel. Moves faster when alone. 
            else 
                vel = clampVector(vel, maxVel);					//Clamp vel to be max vel. Moves faster when alone. 
                                                                                        //std::cout << "\nVel: " << vel.x << ", " << vel.y;
			pos += vel * dt;								//Update position.
			body.setPosition(pos);
			rangeMarker.setPosition(pos);
			delete[] birdWatch;								//Delete the vector arrays from memory to make space for the ones created in the next frame.
			delete[] birdFlight;
		}


		void draw()
		{
			//window.draw(rangeMarker);
			window.draw(body);

		}

		sf::Vector2f getPos()
		{	return pos;	}

		sf::Vector2f getVel()
		{	return vel;	}

};


class Flock
{
	private:
		Boid birds[birdCount];

	public:
		void update()
		{
			for (int i = 0; i < birdCount; i++)
			{
				sf::Vector2f allBirds[birdCount];
				sf::Vector2f allBirdsVel[birdCount];

				int c1 = 0;
				int c2 = 0;

				for(int j = 0; j < birdCount; j++)
				{
					if( i != j && isWithinRange(birds[i].getPos(),  birds[j].getPos()) )                 // used to have i != j, but caused problems.                
					{
						allBirds[j] = shortestPath(birds[i].getPos(), birds[j].getPos());						//Within range of bird i.
						allBirdsVel[j] = birds[j].getVel();
						c1++;
					}
					else
					{
						allBirds[j] = sf::Vector2f(-1, -1);			//Not within range of bird i.
						allBirdsVel[j] = sf::Vector2f(-1, -1);
					}
				}
				
				sf::Vector2f* birdWatch = new sf::Vector2f[c1];
				sf::Vector2f* birdFlight = new sf::Vector2f[c1];

				for (int a = 0; a < c1; a++)
				{
					if(allBirds[a] != sf::Vector2f(-1, -1))
					{
						birdWatch[c2] = allBirds[a];
						birdFlight[c2] = allBirdsVel[a];
						c2++;
					}
				}

				birds[i].update(c1, birdWatch, birdFlight);
			}
		}

		void draw()
		{
			for (int i = 0; i < birdCount; i++)
			{

				birds[i].draw();
			}
		}

};



int main ()		  														
{

	srand(static_cast<int>(time(NULL)));
	window.create(sf::VideoMode(windowWidth, windowHeight), "TITLE", //opens the window and sets the size.
						 sf::Style::Titlebar | sf::Style::Close);	
	window.setFramerateLimit(50);


	if(!font.loadFromFile("FONT.ttf"))					//Loads font from file. Gives error in console if fails.
	{
		std::cout << "\nUnable to load font  \"FONT.fft\".";
	}

	Flock lotsOfBirds;

	//update()
	while(window.isOpen())
	{
		if (timot > 0)
		{
			timot--;
		}

		dt = deltatime.getElapsedTime().asSeconds();
		deltatime.restart();


		sf::Event event;
		while(window.pollEvent(event))									
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))				// ESC to quit.
			{
				window.close();
			}
			if (timot <= 0)
			{
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))				// ESC to quit.
				{
					if (repultion)repultion = false;
					else repultion = true;
					std::cout << "\nRepiltion: " << repultion;
					timot = buttonTimeout;

				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::G))				// ESC to quit.
				{
					if (gravity)gravity = false;
					else gravity = true;
					std::cout << "\nGravity: " << gravity;
					timot = buttonTimeout;

				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::N))				// ESC to quit.
				{
					if (noise) noise = false;
					else noise = true;
					std::cout << "\nNoise: " << noise;
					timot = buttonTimeout;

				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))				// ESC to quit.
				{
					if (align) align = false;
					else align = true;
					std::cout << "\nAlign: " << align;
					timot = buttonTimeout;

				}
			}
			if(event.type == sf::Event::Closed)								//If the event happening is closed:
			{															//then close the window as well.
				window.close();
			}
		}

		window.clear();

		lotsOfBirds.update();

		lotsOfBirds.draw();
		window.display();

	} 

	std::cout << "\n\n";				
	return 0;
}

