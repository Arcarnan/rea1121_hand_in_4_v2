#include <SFML/Graphics.hpp>		//for Graphics stuff
#include <iostream>			//Used for printing to screen(debug) and some input.
#include <cstdlib>			//Random number generator. Rand().


const int windowHeight = 1000; 			//Must be an odd nuber in order to have a center pixle:
const int windowWidth = 1000;

const int mapHeight = 400; 			//Must be an odd nuber in order to have a center pixle:
const int mapWidth = 400;


const float d = 2;								//Added to each node in order to keep some consistancy to the map results.
const int randomRange = 40;					//The range that random number can be generated from as the map is calculated.
const float reductionRate = 0.7f;


sf::ContextSettings settings;					//The settings Context, all the config info the window needs to run.
sf::RenderWindow window;						//The window that draws the graphics on the screen.
sf::Font font;									//The font imported from file used to print text on Screen.
sf::View cam(sf::Vector2f(0,0), sf::Vector2f(mapWidth, mapHeight));
sf::Image map;								//The 2D Texture the algorithm will print onto.
sf::Texture texture;
sf::Sprite disp;								//The Sprite the map is attached to, used to draw the map.
float de = 0;


void drawAlpha(sf::Vector2i pos, int alpha = 0)
{
	alpha += (std::rand() % (randomRange*2)) - randomRange + de;			//Random is changed ######################################
	map.setPixel(pos.x, pos.y, sf::Color(255,255,255, 255-alpha));		//black white
	//map.setPixel(pos.x, pos.y, sf::Color((255-alpha)/4, (255-alpha)/5, 255, 255-alpha));	//color
}


void diamondStep(sf::Rect <int> fld = sf::Rect <int>(0,0, 0,0))
{
	int alpha = 0;
	sf::Vector2i pos(fld.left/2 + fld.width/2, fld.top/2 + fld.height/2);

	alpha = (map.getPixel(fld.left, fld.top).a + 		map.getPixel(fld.width, fld.top).a +
			 map.getPixel(fld.left, fld.height).a + 	map.getPixel(fld.width, fld.height).a) / 4;

	drawAlpha(pos, alpha);
}


void squareStep(sf::Rect <int> fld)
{
	sf::Vector2i pos(0,0);
	int alpha = 0;
	int step = 0;	//how many pixles from the center of the field to one of the edgs

	step = (fld.width - fld.left) /2;

	pos = sf::Vector2i(fld.left/2 + fld.width/2, fld.top/2 + fld.height/2);	//Set pos to be center of field.
	pos.x += step;
	alpha = (map.getPixel(fld.left, fld.top).a + map.getPixel(fld.width, fld.top).a + map.getPixel(fld.width/2, fld.height/2).a) / 3;
	drawAlpha(pos, alpha);

	pos = sf::Vector2i(fld.left/2 + fld.width/2, fld.top/2 + fld.height/2);	//Set pos to be center of field.
	pos.x -= step;
	alpha = (map.getPixel(fld.left, fld.height).a + map.getPixel(fld.width, fld.height).a + map.getPixel(fld.width/2, fld.height/2).a) / 3;
	drawAlpha(pos, alpha);

	pos = sf::Vector2i(fld.left/2 + fld.width/2, fld.top/2 + fld.height/2);	//Set pos to be center of field.
	pos.y += step;	
	alpha = (map.getPixel(fld.left, fld.top).a + map.getPixel(fld.left, fld.height).a + map.getPixel(fld.width/2, fld.height/2).a) / 3;
	drawAlpha(pos, alpha);

	pos = sf::Vector2i(fld.left/2 + fld.width/2, fld.top/2 + fld.height/2);	//Set pos to be center of field.
	pos.y -= step;	
	alpha = (map.getPixel(fld.width, fld.top).a + map.getPixel(fld.width, fld.height).a + map.getPixel(fld.width/2, fld.height/2).a) / 3;
	drawAlpha(pos, alpha);
}


void diamondSquare()
{
	map.create(mapWidth+1, mapHeight+1);		//Intention is to clear the texture before every run. Not sure i create() does this.	
	std::srand(time(NULL));								//Seed the RNG with system time.											
												//Sets all the corners and center to their initial values:

	de = d;										// Since we cant change the constant, we set our variable in main.

	int topCorners = 0;
	int bottomCorners = 0;
	topCorners = (std::rand() % randomRange) + de;
	bottomCorners = (std::rand() % randomRange) + de;

	map.setPixel(0, 0, sf::Color((255-topCorners)/4, (255-topCorners)/5, 255, 255-topCorners));	//color
	map.setPixel(mapWidth, 0, sf::Color((255-topCorners)/4, (255-topCorners)/5, 255, 255-topCorners));	//color
	map.setPixel(0, mapHeight, sf::Color((255-bottomCorners)/4, (255-bottomCorners)/5, 255, 255-bottomCorners));	//color
	map.setPixel(mapWidth, mapHeight, sf::Color((255-bottomCorners)/4, (255-bottomCorners)/5, 255, 255-bottomCorners));	//color

	{
		sf::Rect <int> field(0, 0, mapWidth, mapHeight);				
		diamondStep(field);
		squareStep(field); 
	}

	/*drawAlpha(sf::Vector2i(0, 0));
	drawAlpha(sf::Vector2i(mapWidth, 0));
	drawAlpha(sf::Vector2i(0, mapHeight));
	drawAlpha(sf::Vector2i(mapWidth, mapHeight));
	*/



	for (int size = mapWidth/2; size > 0; size /= 2)				// loops deeper an deeper on the pixel level.
	{
		for(int i = 0; i < mapWidth/size -1; i++)					// repeats the steps vertically.
		{
			for (int j = 0; j < mapWidth/size -1; j++)				// repeats the steps horizontally.
			{
				sf::Rect <int> field((size)*i, (size)*j, size+size*i, size+size*j);				
				diamondStep(field);
				squareStep(field); 
			}
		}
		std::cout <<	"\nde= " << de << "\tsize= " << size << "\n\n\n\n";
		de *= reductionRate;
	}

	texture.loadFromImage(map);
	disp.setTexture(texture);							//Sets the texture for the sprite.

}



int main ()		  														
{
	cam.setCenter(mapWidth/2, mapHeight/2);	
	window.create(sf::VideoMode(windowWidth+1, windowHeight+1), "Diamond Square"); //opens the window and sets the size.
	window.setView(cam);			
	window.setFramerateLimit(30);				//Sets a framelimit to avoid unnessesary resource demandns.	

	if(!font.loadFromFile("FONT.ttf"))					//Loads font from file. Gives error in console if fails.
	{
		std::cout << "\nUnable to load font  \"FONT.fft\".";
	}

	
	
	diamondSquare();

	//update()
	while(window.isOpen())
	{

		window.clear();
		window.draw(disp);
		window.display();

		sf::Event event;
		while(window.pollEvent(event))									
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))				// ESC to quit.
			{
				window.close();
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))				// ESC to quit.
			{
					diamondSquare();
			}

			if(event.type == sf::Event::Closed)								//If the event happening is closed:
			{															//then close the window as well.
				window.close();
			}
		}

	} 

	std::cout << "\n\n";				
	return 0;
}

